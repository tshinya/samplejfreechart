package main;

import java.awt.GridLayout;

import javax.swing.JFrame;

import component.BarSampleChart;
import component.ErrorBarSampleChart;
import component.XYAreaSampleChart;
import component.XYLineSampleChart;
import component.LineSampleChart;
import component.ScatterPlotSampleChart;

public class MainFrame extends JFrame {
	
	public MainFrame() {
		super("JFreeSample");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new GridLayout(2, 3));
		ScatterPlotSampleChart scatterPlot = new ScatterPlotSampleChart();
		LineSampleChart lineChart = new LineSampleChart();
		BarSampleChart barChart = new BarSampleChart();
		ErrorBarSampleChart errorBarChart = new ErrorBarSampleChart();
		XYLineSampleChart xyLineChart = new XYLineSampleChart();
		XYAreaSampleChart xyAreaChart = new XYAreaSampleChart();
		
		this.add(scatterPlot.getChartPanel());
		this.add(lineChart.getChartPanel());
		this.add(barChart.getChartpanel());
		this.add(errorBarChart.getChartPanel());
		this.add(xyLineChart.getChartPanel());
		this.add(xyAreaChart.getChartPanel());
	}

}
