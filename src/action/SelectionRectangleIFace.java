package action;

public interface SelectionRectangleIFace {

	public int select(double x);
	public void cancel();
	public boolean isSelected();
	public final int LOWER_BOUND = 0;
	public final int UPPER_BOUND = 180;
	public final String SELECT_START = "SELECT_START";
	public final String SELECT_END = "SELECT_END";

}
