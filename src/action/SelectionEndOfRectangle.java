package action;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class SelectionEndOfRectangle implements SelectionRectangleIFace{
	
	private XYSeriesCollection data;
	private XYSeries end;
	private boolean selectFlag;
	
	public SelectionEndOfRectangle(XYSeriesCollection _data) {
		this.data = _data;
		this.selectFlag = false;
		end = new XYSeries(SELECT_END);
	}

	@Override
	public int select(double x) {
		selectFlag = true;
		end.add(x, LOWER_BOUND);
		end.add(x, UPPER_BOUND);
		data.addSeries(end);
		int index = data.getSeriesIndex(end.getKey());
		return index;
	}

	@Override
	public void cancel() {
		selectFlag = false;
		data.removeSeries(end);
		this.end.clear();
	}

	@Override
	public boolean isSelected() {
		return this.selectFlag;
	}

}