package action;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class SelectionStartOfRectangle implements SelectionRectangleIFace {

	private XYSeriesCollection data;
	private XYSeries start;
	private boolean selectFlag;
	
	public SelectionStartOfRectangle(XYSeriesCollection _data) {
		this.data = _data;
		this.selectFlag = false;
		start = new XYSeries(SELECT_START);
	}
	
	@Override
	public int select(double x) {
		selectFlag = true;
		start.add(x, LOWER_BOUND);
		start.add(x, UPPER_BOUND);
		data.addSeries(start);
		int index = data.getSeriesIndex(start.getKey());
		return index;
	}

	@Override
	public void cancel() {
		selectFlag = false;
		data.removeSeries(start);
		this.start.clear();
	}

	@Override
	public boolean isSelected() {
		return this.selectFlag;
	}
}