package action;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;

import component.XYLineSampleChart;

public class SelectionRectangleMouseAdapter implements ChartMouseListener {

	XYLineSampleChart chart;
	
	public SelectionRectangleMouseAdapter(XYLineSampleChart _chart) {
		this.chart = _chart;
	}
	
	@Override
	public void chartMouseClicked(ChartMouseEvent e) {
		if (e.getEntity().getToolTipText() == null) {
			return ;
		}
		
		String str = e.getEntity().getToolTipText();
		int start = 0, end = 0;
		if (((start = str.indexOf("(")) == -1) || ((end = str.indexOf(")")) == -1)) {
			return ;
		}
		
		str = str.substring(start+1, end);
		String[] range = str.split(", ");
		
		double x = Double.valueOf(range[0]);
		double y = Double.valueOf(range[1]);
		chart.draw(x);
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent e) {
		// TODO Auto-generated method stub

	}

}
