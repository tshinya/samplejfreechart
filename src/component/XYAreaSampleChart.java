package component;

import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class XYAreaSampleChart {

	private ChartPanel chartPanel;
	
	public XYAreaSampleChart() {
		JFreeChart chart = ChartFactory.createXYAreaChart("XYAreaSampleChart", "X-Label", "Y-Label", setData(), PlotOrientation.VERTICAL, true, true, false);
		chart.setBackgroundPaint(ChartColor.WHITE);
		// 表示域を自動で変更してくれる.
		chart.getXYPlot().getRangeAxis().setAutoRange(true);
		// 表示域を指定
		// chart.getXYPlot().getRangeAxis().setRange(-800.0, 800.0);
		chartPanel = new ChartPanel(chart);
	}

	private XYDataset setData() {
		XYSeries xyseries = new XYSeries("Group-A");
		xyseries.add(1, 500.19999999999999D);
		xyseries.add(2, 694.10000000000002D);
		xyseries.add(3, -734.39999999999998D);
		xyseries.add(4, 453.19999999999999D);
		xyseries.add(5, 500.19999999999999D);
		XYSeries xyseries1 = new XYSeries("Group-B");
		xyseries1.add(1, 700.20000000000005D);
		xyseries1.add(2, 534.10000000000002D);
		xyseries1.add(3, 323.39999999999998D);
		xyseries1.add(4, 125.2D);
		xyseries1.add(5, 653.20000000000005D);
		XYSeries xyseries2 = new XYSeries("Group-C");
		xyseries2.add(1, 480.20000000000005D);
		xyseries2.add(2, 204.10000000000002D);
		xyseries2.add(3, 123.39999999999998D);
		xyseries2.add(4, 195.2D);
		xyseries2.add(5, 453.20000000000005D);
		XYSeries xyseries3 = new XYSeries("Group-D");
		xyseries3.add(1, 180.20000000000005D);
		xyseries3.add(2, 104.10000000000002D);
		xyseries3.add(3, 183.39999999999998D);
		xyseries3.add(4, 195.2D);
		xyseries3.add(5, 253.20000000000005D);
		XYSeriesCollection xyseriescollection = new XYSeriesCollection();
		xyseriescollection.addSeries(xyseries);
		xyseriescollection.addSeries(xyseries1);
		xyseriescollection.addSeries(xyseries2);
		xyseriescollection.addSeries(xyseries3);
		return xyseriescollection;
	}
	
	public ChartPanel getChartPanel() {
		return this.chartPanel;
	}
	
}