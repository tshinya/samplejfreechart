package component;

import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class BarSampleChart {
	
	private ChartPanel chartPanel;

	public BarSampleChart() {
		JFreeChart chart = ChartFactory.createBarChart("BarSampleChart", "X-Label", "Y-Label", setData(), PlotOrientation.VERTICAL, true, false, false);
		chart.setBackgroundPaint(ChartColor.WHITE);
		setVerticalAxis(chart);
		chartPanel = new ChartPanel(chart);
	}
	
	public ChartPanel getChartpanel() {
		return this.chartPanel;
	}
	
	private void setVerticalAxis(JFreeChart chart) {
		CategoryPlot plot = chart.getCategoryPlot();
		NumberAxis numberAxis = (NumberAxis) plot.getRangeAxis();
		numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		numberAxis.setLowerBound(0); // Set minimum value
		numberAxis.setUpperBound(50); // Set maximum value
	}
	
	private DefaultCategoryDataset setData() {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		// dataset.addValue( Vertical-Axis , Group name, Horizontal-Axis);
		dataset.addValue(10 , "Line-A", "GroupA");
		dataset.addValue(20 , "Line-B", "GroupA");
		dataset.addValue(30 , "Line-C", "GroupA");
		
		return dataset;
	}
}
