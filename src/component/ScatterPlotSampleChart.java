package component;

import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class ScatterPlotSampleChart {
	
	private ChartPanel panel;
	
	public ScatterPlotSampleChart() {
		JFreeChart chart = ChartFactory.createScatterPlot("ScatterPlotSample", "X-Label", "Y-Label", setData(), PlotOrientation.VERTICAL, true, false, false);
		chart.setBackgroundPaint(ChartColor.WHITE);
		panel = new ChartPanel(chart);
	}
	
	public ChartPanel getChartPanel() {
		return this.panel;
	}

	private IntervalXYDataset setData() {
		XYSeriesCollection data = new XYSeriesCollection();

		int xdata1[] = {10, 15, 20, 22, 24, 26,  4,  8, 28, 30, 25, 12, 100};
		int ydata1[] = {26, 42, 54, 56, 52, 58, 20, 24, 51, 49, 54, 38, 44};
		int xdata2[] = {29, 14, 12, 20, 28, 35, 32, 31, 25};
		int ydata2[] = {52,  7,  6,  5, 56, 64, 58, 62, 30};

		XYSeries series1 = new XYSeries("Group-A");
		XYSeries series2 = new XYSeries("Group-B");

		for (int i = 0 ; i < xdata1.length ; i++){
			series1.add(xdata1[i], ydata1[i]);
		}
		for (int i = 0 ; i < ydata2.length ; i++){
			series2.add(xdata2[i], ydata2[i]);
		}
		data.addSeries(series1);
		data.addSeries(series2);

		return data;
	}
}