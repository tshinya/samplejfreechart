package component;

import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

public class LineSampleChart {
	
	private ChartPanel panel;
	
	public LineSampleChart() {
		JFreeChart chart = ChartFactory.createLineChart("LineSampleChart", "X-Label", "Y-Label", setData(), PlotOrientation.VERTICAL, true, false, false);
		chart.setBackgroundPaint(ChartColor.WHITE);
		setVerticalAxis(chart);
		panel = new ChartPanel(chart);
	}
	
	public ChartPanel getChartPanel() {
		return this.panel;
	}
	
	private void setVerticalAxis(JFreeChart chart) {
		CategoryPlot plot = chart.getCategoryPlot();
		NumberAxis numberAxis = (NumberAxis) plot.getRangeAxis();
		numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		numberAxis.setLowerBound(0); // Set minimum value
		numberAxis.setUpperBound(100); // Set maximum value
		
	}

	private DefaultCategoryDataset setData() {
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		for (int i = 0; i < 10; i++) {
			// dataset.addValue( Vertical-Axis , group, Horizontal-Axis);
			dataset.addValue(i , "Line-A", String.valueOf(i));
			dataset.addValue((i*5)%13 , "Line-B", String.valueOf(i));
			dataset.addValue((i*10)%19 , "Line-C", String.valueOf(i));
		}
		
		return dataset;
	}
}