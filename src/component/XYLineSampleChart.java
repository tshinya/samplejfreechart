package component;

import java.awt.Color;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.axis.TickUnit;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import action.SelectionEndOfRectangle;
import action.SelectionRectangleIFace;
import action.SelectionRectangleMouseAdapter;
import action.SelectionStartOfRectangle;

public class XYLineSampleChart {

	private ChartPanel chartPanel;
	private final int INTERVAL = 30;
	private SelectionStartOfRectangle startRange;
	private SelectionEndOfRectangle endRange;
	
	public XYLineSampleChart() {
		XYSeriesCollection data = setData();
		// createXYLineChart(); 第6引数は凡例の表示の有無, 第7引数はツールチップの表示の有無
		JFreeChart chart = ChartFactory.createXYLineChart("XYLineSampleChart", "X-Label", "Y-Label", data, PlotOrientation.VERTICAL, false, true, false);
		chart.setBackgroundPaint(ChartColor.WHITE);
		setValueAxis(chart);
		startRange = new SelectionStartOfRectangle(data);
		endRange = new SelectionEndOfRectangle(data);
		chartPanel = new ChartPanel(chart);
		chartPanel.addChartMouseListener(new SelectionRectangleMouseAdapter(this));
	}
	
	private void setValueAxis(JFreeChart chart) {
		XYPlot plot = chart.getXYPlot();
		ValueAxis valueAxis = plot.getDomainAxis();
		valueAxis.setLowerBound(SelectionRectangleIFace.LOWER_BOUND);
		valueAxis.setUpperBound(SelectionRectangleIFace.UPPER_BOUND);
		TickUnits units = new TickUnits();
		TickUnit unit = new NumberTickUnit(INTERVAL);
		units.add(unit);
		valueAxis.setStandardTickUnits(units);
	}
	
	public ChartPanel getChartPanel() {
		return this.chartPanel;
	}

	private XYSeriesCollection setData() {
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries lineA = new XYSeries("Line-A");
		XYSeries lineB = new XYSeries("Line-B");
		XYSeries lineC = new XYSeries("Line-C");
		for (int i = 0; i < 180; i++) {
			// .add(Horizontal-Axis, Vertical-Axis);
			lineA.add(i, i);
			lineB.add(i, (i*5)%130);
			lineC.add(i, (i*10)%190);
		}
		dataset.addSeries(lineA);
		dataset.addSeries(lineB);
		dataset.addSeries(lineC);
		return dataset;
	}
	
	public void draw(double x) {
		if (!startRange.isSelected()) {
			drawSelectedRectangle(startRange, x);
			return ;
		}
		if (!endRange.isSelected()) {
			drawSelectedRectangle(endRange, x);
			return ;
		}
		resetSelectedRectangle();
	}
	
	private void drawSelectedRectangle(SelectionRectangleIFace line, double x) {
		int index = line.select(x);
		XYPlot plot = chartPanel.getChart().getXYPlot();
		plot.getRenderer().setSeriesPaint(index, Color.BLACK);
	}
	
	private void resetSelectedRectangle() {
		endRange.cancel();
		startRange.cancel();
	}
}