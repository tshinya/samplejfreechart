package component;

import java.awt.BasicStroke;
import java.awt.Color;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.StatisticalBarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.statistics.DefaultStatisticalCategoryDataset;

public class ErrorBarSampleChart {

	private ChartPanel chartPanel;
	
	public ErrorBarSampleChart() {
		JFreeChart chart = ChartFactory.createLineChart("ErrorBarSampleChart", "X-Label", "Y-Label", setDataset(), PlotOrientation.VERTICAL, true, false, false);
		StatisticalBarRenderer renderer = new StatisticalBarRenderer();
		renderer.setErrorIndicatorPaint(Color.GREEN); // エラーバーの色を指定
		renderer.setErrorIndicatorStroke(new BasicStroke(2)); // エラーバーの太さを指定
		CategoryPlot categoryPlot = (CategoryPlot) chart.getPlot();
		categoryPlot.setRenderer(renderer);
		chartPanel = new ChartPanel(chart);
	}
	
	public ChartPanel getChartPanel() {
		return this.chartPanel;
	}
	
	private CategoryDataset setDataset() {
		DefaultStatisticalCategoryDataset dataset = new DefaultStatisticalCategoryDataset();
		
		// dataset.add(平均値, 標準偏差, "Row-1", "Column-1");
		dataset.add(10D, 2.3999999999999999D, "Row-1", "Column-1");
		dataset.add(15D, 4.4000000000000004D, "Row-1", "Column-2");
		dataset.add(13D, 2.1000000000000001D, "Row-1", "Column-3");
		dataset.add(22D, 2.3999999999999999D, "Row-2", "Column-1");
		dataset.add(18D, 4.4000000000000004D, "Row-2", "Column-2");
		dataset.add(28D, 2.1000000000000001D, "Row-2", "Column-3");
		return dataset;
	}
}